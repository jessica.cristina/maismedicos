package model;

public class Paciente extends Usuario {
	
	private boolean gravidade;
	private boolean prioridade; // true = Alta, false = baixa;
	ConsultaMedica situacao;

	public boolean getGravidade() {
		return gravidade;
	}

	public void setGravidade(boolean gravidade) {
		this.gravidade = gravidade;
	}

	public Boolean getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(Boolean prioridade) {
		this.prioridade = prioridade;
	}

	public ConsultaMedica getSituacao() {
		return situacao;
	}

	public void setSituacao(ConsultaMedica situacao) {
		this.situacao = situacao;
	}

}
