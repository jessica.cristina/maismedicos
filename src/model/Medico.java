package model;

public class Medico extends Usuario{
	private int indentificao;
	private String areaAtuacao;
	private ConsultaMedica infoPaciente;
	public int getIndentificao() {
		return indentificao;
	}
	public void setIndentificao(int indentificao) {
		this.indentificao = indentificao;
	}
	public String getAreaAtuacao() {
		return areaAtuacao;
	}
	public void setAreaAtuacao(String areaAtuacao) {
		this.areaAtuacao = areaAtuacao;
	}
	public ConsultaMedica getInfoPaciente() {
		return infoPaciente;
	}
	public void setInfoPaciente(ConsultaMedica infoPaciente) {
		this.infoPaciente = infoPaciente;
	}

	
	

}
