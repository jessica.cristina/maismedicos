package model;
import model.Paciente;

public class ConsultaMedica {
	
	private String nomeMedico;
	private boolean internacao;
	private String detalhesSituacao;

	public boolean getInternacao() {
		return internacao;
	}

	public void setInternacao(Paciente umPaciente) {
		this.internacao = umPaciente.getGravidade();
	}

	public String getNomeMedico() {
		return nomeMedico;
	}

	public void setNomeMedico(String nomeMedico) {
		this.nomeMedico = nomeMedico;
	}

	public String getDetalhesSituacao() {
		return detalhesSituacao;
	}

	public void setDetalhesSituacao(String detalhesSituacao) {
		this.detalhesSituacao = detalhesSituacao;
	}
	

}
