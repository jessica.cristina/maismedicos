package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.JToggleButton;

public class MaisMedicos extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_1;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MaisMedicos frame = new MaisMedicos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MaisMedicos() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 517, 571);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(75, 51, 114, 19);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel label = new JLabel("");
		label.setBounds(22, 113, 70, 15);
		contentPane.add(label);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(22, 24, 70, 15);
		contentPane.add(lblUsuario);
		
		JLabel lblRg = new JLabel("RG:");
		lblRg.setBounds(22, 113, 70, 15);
		contentPane.add(lblRg);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(22, 51, 70, 15);
		contentPane.add(lblNome);
		
		JLabel lblIdade = new JLabel("Idade:");
		lblIdade.setBounds(22, 82, 70, 15);
		contentPane.add(lblIdade);
		
		textField_2 = new JTextField();
		textField_2.setBounds(75, 113, 114, 19);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblPaciente = new JLabel("Paciente");
		lblPaciente.setBounds(47, 289, 70, 15);
		contentPane.add(lblPaciente);
		
		textField_3 = new JTextField();
		textField_3.setBounds(94, 322, 114, 19);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblSituacao = new JLabel("Situacao:");
		lblSituacao.setBounds(12, 324, 70, 15);
		contentPane.add(lblSituacao);
		
		JLabel lblPrioridade = new JLabel("Prioridade:");
		lblPrioridade.setBounds(12, 351, 80, 15);
		contentPane.add(lblPrioridade);
		
		textField_4 = new JTextField();
		textField_4.setBounds(94, 349, 114, 19);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblMedico = new JLabel("Medico");
		lblMedico.setBounds(47, 398, 70, 15);
		contentPane.add(lblMedico);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(85, 82, 32, 24);
		contentPane.add(comboBox);
		
		JLabel lblIdentificacao = new JLabel("Identificacao:");
		lblIdentificacao.setBounds(25, 425, 105, 15);
		contentPane.add(lblIdentificacao);
		
		textField_1 = new JTextField();
		textField_1.setBounds(148, 423, 114, 19);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblAreaDeAtuacao = new JLabel("Area de atuacao:");
		lblAreaDeAtuacao.setBounds(22, 452, 132, 15);
		contentPane.add(lblAreaDeAtuacao);
		
		textField_5 = new JTextField();
		textField_5.setBounds(148, 450, 114, 19);
		contentPane.add(textField_5);
		textField_5.setColumns(10);
		
		JToggleButton tglbtnEnviar = new JToggleButton("Enviar");
		tglbtnEnviar.setBounds(12, 509, 167, 25);
		contentPane.add(tglbtnEnviar);
		
		JLabel lblBuscar = new JLabel("Buscar:");
		lblBuscar.setBounds(22, 173, 70, 15);
		contentPane.add(lblBuscar);
		
		textField_6 = new JTextField();
		textField_6.setBounds(110, 171, 114, 19);
		contentPane.add(textField_6);
		textField_6.setColumns(10);
		
		textField_7 = new JTextField();
		textField_7.setBounds(110, 198, 114, 19);
		contentPane.add(textField_7);
		textField_7.setColumns(10);
		
		JLabel lblAdicionar = new JLabel("Adicionar:");
		lblAdicionar.setBounds(22, 200, 95, 15);
		contentPane.add(lblAdicionar);
		
		JLabel lblRemover = new JLabel("Remover:");
		lblRemover.setBounds(22, 227, 70, 15);
		contentPane.add(lblRemover);
		
		textField_8 = new JTextField();
		textField_8.setBounds(110, 229, 114, 19);
		contentPane.add(textField_8);
		textField_8.setColumns(10);
	}
}
