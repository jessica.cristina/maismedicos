package view;
import java.util.ArrayList;

import model.Medico;
import model.Paciente;

public class ControleUsuario {
	
	ArrayList<Medico> listaMedico;
	ArrayList<Paciente> listaPaciente;

	public ControleUsuario() {
		listaMedico = new ArrayList<Medico>();
		listaPaciente = new ArrayList<Paciente>();
	}

	public void Adicionar(Medico umMedico) {
		listaMedico.add(umMedico);
	}

	public void Adicionar(Paciente umPaciente) {
		listaPaciente.add(umPaciente);
	}

	public void Remover(Medico umMedico) {
		listaMedico.remove(umMedico);
	}

	public void Remover(Paciente umPaciente) {
		listaPaciente.remove(umPaciente);
	}
	
	public Medico BuscarMedico(String umNome) {
		for(Medico umMedico: listaMedico) {
			if(umMedico.getNome().equalsIgnoreCase(umNome)) {
				return 	umMedico;
			}
		}
		return null;
	}
	
	public Paciente BuscarPaciente(String umNome) {
		for(Paciente umPaciente: listaPaciente) {
			if(umPaciente.getNome().equalsIgnoreCase(umNome)) {
				return 	umPaciente;
			}
		}
		return null;
	}

}
