package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.JToggleButton;

public class ControleConsulta extends JFrame {

	private JPanel contentPane;
	private JTextField textField_1;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ControleConsulta frame = new ControleConsulta();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ControleConsulta() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);setBounds(100, 100, 450, 354);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblConsulta = new JLabel("Consulta");
		lblConsulta.setBounds(12, 12, 70, 15);
		contentPane.add(lblConsulta);
		
		JLabel lblMedicoQueConsultou = new JLabel("Medico que consultou:");
		lblMedicoQueConsultou.setBounds(12, 51, 174, 15);
		contentPane.add(lblMedicoQueConsultou);
		
		textField_1 = new JTextField();
		textField_1.setBounds(180, 49, 114, 19);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblPacienteConsultado = new JLabel("Paciente consultado:");
		lblPacienteConsultado.setBounds(12, 78, 163, 15);
		contentPane.add(lblPacienteConsultado);
		
		textField = new JTextField();
		textField.setBounds(180, 78, 114, 19);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblPacienteFoiInternado = new JLabel("Paciente foi internado:");
		lblPacienteFoiInternado.setBounds(22, 115, 193, 15);
		contentPane.add(lblPacienteFoiInternado);
		
		JRadioButton rdbtnSim = new JRadioButton("Sim");
		rdbtnSim.setBounds(26, 144, 149, 23);
		contentPane.add(rdbtnSim);
		
		JRadioButton rdbtnNao = new JRadioButton("Nao");
		rdbtnNao.setBounds(204, 144, 149, 23);
		contentPane.add(rdbtnNao);
		
		JLabel lblGravidade = new JLabel("Gravidade:");
		lblGravidade.setBounds(12, 187, 92, 15);
		contentPane.add(lblGravidade);
		
		JRadioButton rdbtnBaixa = new JRadioButton("Baixa");
		rdbtnBaixa.setBounds(8, 214, 149, 23);
		contentPane.add(rdbtnBaixa);
		
		JRadioButton rdbtnIntermediario = new JRadioButton("Intermediario");
		rdbtnIntermediario.setBounds(161, 214, 149, 23);
		contentPane.add(rdbtnIntermediario);
		
		JRadioButton rdbtnAlta = new JRadioButton("Alta");
		rdbtnAlta.setBounds(336, 214, 149, 23);
		contentPane.add(rdbtnAlta);
		
		JLabel lblHoraDoAtendimento = new JLabel("Hora do atendimento:");
		lblHoraDoAtendimento.setBounds(22, 260, 179, 15);
		contentPane.add(lblHoraDoAtendimento);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(219, 255, 32, 24);
		contentPane.add(comboBox);
		
		JToggleButton tglbtnEnviarRelatorio = new JToggleButton("Enviar relatorio");
		tglbtnEnviarRelatorio.setBounds(19, 304, 167, 25);
		contentPane.add(tglbtnEnviarRelatorio);
	}
}
